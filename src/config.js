module.exports = {
    appName: "testest",
    basePath: "https://www.benarnews.org",
    enableCategories: false,
    accentColor: "#6699cc",
    analytics: {
        type: "console",
        enabled: false,
        config: {
        }
    },
    flavors: {
        default: {
            name: "mainfeed",
            translationKey: "en",
            defaultForLanguages: ["en"],
            cssFile: "./assets/css/default.css",
            url: "https://benarnews.org/english/appcats/this-is-the-main-feed/mainfeed/app_main_rss",
            isRTL: false
        }
    }
}
